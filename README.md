TL Lunch Chooser
===================


A simple app to submit choices which Logged in users can vote for until a set date.

----------

## Techs Used ##

 - VueJs (plus VueResource/VueRouter)
 - Webpack (babel, html-webpack-plugin, hot reload, bootstrap-webpack)
 - auth0/auth0-lock
 - Firebase
 - MomentJs
 - flatpickr
 - x-editable