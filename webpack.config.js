var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // the main entry of our app
  entry: ['./src/index.js'],
  // output configuration

  devtool: "source-map",

  output: {
    path: path.join(__dirname, 'dist'),
    filename: '/main.js'
  },

  devServer: {
    hot: true,
    historyApiFallback: true
  },

  module: {
    loaders: [
      { test: /\.vue$/, loader: 'vue' },
      { test: /\.js$/, loader: 'babel', exclude: /(node_modules|bootstrap-webpack)/ },
      { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' },
      { test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
      { test: /node_modules[\\\/]auth0-lock[\\\/].*\.js$/, loaders: ['transform-loader/cacheable?brfs', 'transform-loader/cacheable?packageify']}, 
      { test: /node_modules[\\\/]auth0-lock[\\\/].*\.ejs$/, loader: 'transform-loader/cacheable?ejsify'}, 
      { test: /\.json$/, loader: 'json-loader'}
    ]
  },

  plugins: [new HtmlWebpackPlugin({
    title: 'TL Lunch Chooser',
    filename: 'index.html',
    template: 'my-index.ejs',
    hash: true
  })],

  babel: {
    presets: ['es2015'],
    plugins: ['transform-runtime']
  }
}