'use strict'

import 'expose?$!expose?jQuery!jquery';
import 'bootstrap-webpack';
import './less/main.less'
window._ = require('underscore')

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Firebase from 'firebase'
import VueFire from 'vuefire'

import Auth from './auth'

import App from './components/App.vue'
import Home from './components/Home.vue'
import SomethingPrivate from './components/SomethingPrivate.vue'
import ListItem from './components/ListItem.vue'
import NewItemModal from './components/NewItemModal.vue'
import ItemView from './components/ItemView.vue'

import moment from 'moment';

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueFire);

//Register components
Vue.component('ListItem', ListItem);
Vue.component('NewItemModal', NewItemModal);
Vue.component('ItemView', ItemView);
Vue.component('SomethingPrivate', SomethingPrivate);

export var router = new VueRouter({history: true});

// Set up routing and match routes to components
router.map({
  '/home': {
    component: Home
  },
  '/items/:id': {
    component: ItemView
  },
  '/something-private': {
    component: SomethingPrivate
  }
})

// Redirect to the home route if any routes are unmatched
router.redirect({
  '*': '/home'
})

// Start the app on the #app div
router.start(App, '#app')