var firebaseConfig = {
  apiKey: "AIzaSyBz9IDQv6FifyyL8gG8BfXFpFXsbkZEOxs",
  authDomain: "tllunchchooser.firebaseapp.com",
  databaseURL: "https://tllunchchooser.firebaseio.com",
  storageBucket: "tllunchchooser.appspot.com",
};

firebase.initializeApp(firebaseConfig);
var firebaseDB = firebase.database();

export default {
    firebaseDB: firebaseDB
}